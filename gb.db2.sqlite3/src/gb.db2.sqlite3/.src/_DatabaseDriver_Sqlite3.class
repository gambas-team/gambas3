' Gambas class file

Export
Inherits _DatabaseDriver

Static Private $aCollations As String[] = ["BINARY", "NOCASE", "RTRIM"]

Private $pHandle As Pointer
Private $sHost As String

Private Sub IsDatabaseFile(sPath As String) As Boolean

  Dim hFile As File
  Dim sMagic As String
  
  hFile = Open sPath
  sMagic = Read #hFile, -15
  Return sMagic == "sqlite format 3"
  
Catch
  
End

Private Sub FindDatabase(sHost As String, sName As String, Optional bCreate As Boolean) As String
  
  Dim sPath As String
  Dim sTemp As String
  
  If Not File.IsRelative(sName) Then
    If bCreate Then 
      If Not Exist(sName) Then Return sName
    Else
      If IsDatabaseFile(sName) Then Return sName
    Endif
  Else
    If Not sHost Then sHost = User.Home
    sPath = sHost &/ sName
    
    If bCreate Then

      If File.IsRelative(sPath) Then Error.Raise("Cannot create database in the project directory")
      If Exist(sPath) Then Error.Raise("Database already exists")

      Return sPath
    
    Else
      
      If File.IsRelative(sPath) Then 
        sPath = "..." &/ sPath
        If IsDatabaseFile(sPath) Then 
          sTemp = Application.TempDir &/ "gb.db2.sqlite3"
          Try Mkdir sTemp
          Try Copy sPath To sTemp &/ File.Name(sPath)
          If Error Then Error.Raise("Cannot copy database file to the temporary directory: " & Error.Text)
          Return sTemp &/ File.Name(sPath)
        Endif
      Endif
      
      If IsDatabaseFile(sPath) Then Return sPath
      
    Endif
  Endif
  
  Error.Raise("Database does not exist")

End

Public Sub Open(Conn As Connection) As Pointer

  Dim sPath As String
  Dim sVersion As String
  Dim aVersion As String[]
  
  $sHost = Conn.Host
  
  If Conn.Name Then
    sPath = File.RealPath(FindDatabase($sHost, Conn.Name))
  Endif
  
  $pHandle = _Sqlite3Helper.Open(sPath)
  If IsNull($pHandle) Then Error.Raise("Unable to open database: " & _Sqlite3Helper.GetErrorMessage(Null))
  
  sVersion = _Sqlite3Helper.GetVersion()
  Me.FullVersion = sVersion

  aVersion = Split(sVersion, ".")
  Me.Version = CInt(aVersion[0]) * 10000 + CInt(aVersion[1]) * 100 + CInt(aVersion[2])
  
  Me.Exec("PRAGMA empty_result_callbacks = ON")
  ' [NG 29/12/2005] 3.2.1 introduced a problem with columns names which is resolved by setting short columns off first.
  If Me.Version < 30803 Then Me.Exec("PRAGMA short_column_names = OFF")
  Me.Exec("PRAGMA full_column_names = ON")
  
  Me.Charset = "UTF-8"
  
  Me.NoTableType = True
  Me.NoTransactionNest = True
  Me.NoReturning = Me.Version < 33500
  Me.InsertIfNotExist = If(Me.Version < 32400, _DatabaseDriver.IGNORE_NONE, _DatabaseDriver.IGNORE_ON_CONFLICT)  
  Me.AllowedCharsInDatabaseName = "."
  Me.LimitPos = _DatabaseDriver.LIMIT_AT_END
  Me.QuoteCharacter = "\""
  Me.MustQuoteBlob = True
  
  Return $pHandle
  
End

Public Sub Close()

  _Sqlite3Helper.Close($pHandle)
  
End

Private Sub DateToUTC({Date} As Date) As Date

  Return {Date} + System.TimeZone / 86400

End

Private Sub DateToISO8601({Date} As Date) As String

  Dim iYear As Integer
  Dim sResult As String
  Dim sMicroseconds As String
  
  {Date} = DateToUTC({Date})

  iYear = Year({Date})
  
  If iYear Then
    
    If iYear < 0 Then sResult = "B"
    sResult &= Format(iYear, "0000") & "-" & Format(Month({Date}), "00") & "-" & Format(Day({Date}), "00")
    
  Endif
  
  sResult &= "T" & Format({Date}, "hhnnss")
  
  sMicroseconds = Format({Date}, "uu")
  If sMicroseconds And If sMicroseconds <> "000" Then sResult &= "." & sMicroseconds
  
  Return sResult & "Z"
  
End

Public Sub Format(Value As Variant, Optional Blob As Boolean) As String

  Select Case TypeOf(Value)
    
    Case gb.Boolean
      Return If(Value, "1", "0")
    
    Case gb.Date
      Return "'" & DateToISO8601(Value) & "'"
      
    Case gb.String
      If Blob Then 
        Return _Sqlite3Helper.QuoteBlob(Value)
      Else
        Return Super.Format(Value)
      Endif
      
    Default
      Return Super.Format(Value)
    
  End Select
  
End

Public Sub Query(Request As String) As Pointer

  Dim pResult As Pointer

  If DB.Debug Then Error "gb.db.sqlite3:";; Request
  pResult = _Sqlite3Helper.Query($pHandle, Request, Me.Timeout, True)
  If IsNull(pResult) Then Error.Raise(String.UCaseFirst(_Sqlite3Helper.GetErrorMessage($pHandle)))
  Return pResult
  
End

Public Sub GetError() As Integer
  
  Return _Sqlite3Helper.GetError($pHandle)
  
End

Public Sub GetResultCount((Result) As Pointer) As Integer

  Return _Sqlite3Helper.GetResultCount(Result)
  
End

Public Sub GetResultField((Result) As Pointer, Field As Integer, ByRef Name As String, ByRef Type As Integer, ByRef Length As Integer) As Boolean

  Name = _Sqlite3Helper.GetResultField(Result, Field)
  If Not Name Then Return True
  
  Type = _Sqlite3Helper.Type
  Length = _Sqlite3Helper.Length
  
End


Public Sub FreeResult((Result) As Pointer)
  
  If IsNull(Result) Then Return
  _Sqlite3Helper.FreeResult(Result)
  
End

Public Sub GetResultData((Result) As Pointer, Index As Integer, Next As Boolean) As Variant[]
  
  Return _Sqlite3Helper.GetResultData(Result, Index, Next)
  
End

Public Sub GetResultBlob((Result) As Pointer, Index As Integer, Field As Integer) As String
  
  Return _Sqlite3Helper.GetResultBlob(Result, Index, Field)
  
End

Public Sub GetTables() As String[]
  
  Dim aTables As New String[]
  Dim pResult As Pointer
  Dim I As Integer
  
  pResult = Query("SELECT tbl_name FROM (SELECT tbl_name FROM sqlite_master WHERE Type = 'table' UNION SELECT tbl_name FROM sqlite_temp_master WHERE type = 'table')")
  
  For I = 0 To GetResultCount(pResult) - 1
    aTables.Add(GetResultData(pResult, I, True)[0])
  Next
  
  FreeResult(pResult)
  
  aTables.Add("sqlite_master")
  aTables.Add("sqlite_temp_master")
  
  Return aTables
  
End

Public Sub IsSystemTable(Table As String) As Boolean
  
  Return Table Begins "sqlite_"
  
End

Public Sub GetTableFields(Table As String) As String[]

  Dim aFields As New String[]
  Dim pResult As Pointer
  Dim I As Integer
  
  pResult = Query("PRAGMA table_info('" & Table & "')")
  
  For I = 0 To GetResultCount(pResult) - 1
    aFields.Add(GetResultData(pResult, I, True)[1])
  Next
  
  FreeResult(pResult)
  
  Return aFields

End

Private Sub GetSchema(Table As String) As String

  Dim pResult As Pointer
  Dim sSchema As String
  
  pResult = Query("SELECT sql FROM sqlite_master WHERE type = 'table' AND tbl_name = '" & Table & "'")
  sSchema = GetResultData(pResult, 0, False)[0]
  FreeResult(pResult)
  Return sSchema
  
End

Public Sub GetTableFieldInfo(Table As String, Field As String, ByRef Type As Integer, ByRef Length As Integer, ByRef {Default} As Variant, ByRef Collation As String)

  Dim pResult As Pointer
  Dim I As Integer
  Dim vRow As Variant[]
  Dim sType As String
  Dim bNotNull As Boolean
  Dim sDefault As String
  Dim sSchema As String
  Dim iPos As Integer
  Dim aSchema As String[]
  Dim sDef As String
  Dim aField As String[]
  Dim sField As String
  Dim bAutoInc As Boolean
  Dim N As Integer

  pResult = Query("PRAGMA table_info('" & Table & "')")
  
  N = GetResultCount(pResult)
  For I = 0 To N - 1
    
    vRow = GetResultData(pResult, I, True)
    
    If vRow[1] = Field Then
      sType = vRow[2]
      bNotNull = vRow[3]
      sDefault = vRow[4]
      Break
    Endif
    
  Next
  
  FreeResult(pResult)
  
  If I >= N Then Error.Raise("Unable to find field: " & Table & "." & Field)
  
  sSchema = Trim(GetSchema(Table))
  
  If sSchema Then
    ' CREATE TABLE "diary" ( "iCodeDiary" INT4 NOT NULL , "sName" TEXT, "sDescription" TEXT, "iLength" INT4 NOT NULL DEFAULT 0, "iTime" INT4 NOT NULL DEFAULT 0, "iWeekDay" INT4 NOT NULL DEFAULT 0, "iDay" INT4 NOT NULL DEFAULT 0, "iMonth" INT4 NOT NULL DEFAULT 0, "iYear" INT4 NOT NULL DEFAULT 0, "dStart" DATETIME, "dEnd" DATETIME, "iCodeShow" INT4 NOT NULL DEFAULT 0, "bRepeat" BOOL NOT NULL DEFAULT '0', "iCodeGroup" INT4 NOT NULL DEFAULT 0, "iCodeFile" INT4 NOT NULL DEFAULT 0, "iCodeJingle" INT4 NOT NULL DEFAULT 0, "sFileList" TEXT, "iRandomType" INT4 NOT NULL DEFAULT 0, "iRandomAfter" INT4 NOT NULL DEFAULT 0, "sTagFilter" TEXT, "bPlayRandomOnce" BOOL NOT NULL DEFAULT '0', "bCheck" BOOL NOT NULL DEFAULT '0', "iColor" INT4, "sWarning" TEXT, "dWarning" DATETIME, PRIMARY KEY ("iCodeDiary") )
  
    iPos = InStr(sSchema, "(")
    If iPos Then
      
      sSchema = Mid$(sSchema, iPos + 1, -1)
      aSchema = Split(sSchema, ",", "\"", True, True)
      
      For Each sDef In aSchema
        aField = Split(Trim(sDef), " ", Chr$(34), True, True)
        sField = aField[0]
        If InStr("'\"`", Left(sField)) And If Right(sField) = Left(sField) Then 
          sField = Mid$(sField, 2, -1)
          If sField == Field Then
            If sType Begins "INT" And If aField.Exist("AUTOINCREMENT", gb.IgnoreCase) Then bAutoInc = True
            iPos = aField.Find("COLLATE", gb.IgnoreCase)
            If iPos >= 0 Then Collation = aField[iPos + 1]
            Break
          Endif
        Endif
      Next
      
    Endif
    
  Endif
  
  Type = _Sqlite3Helper.GetType(sType)
  Length = _Sqlite3Helper.Length
  
  If bNotNull Then 
    If sDefault Begins "'" Then sDefault = Replace(Mid$(sDefault, 2, -1), "''", "'")
    If sDefault Then {Default} = _Sqlite3Helper.GetValue(sDefault, Type)
  Endif
  
  If bAutoInc Then Type = db.Serial
    
End

Public Sub GetTableIndexes(Table As String) As String[]

  Dim aIndexes As New String[]
  Dim pResult As Pointer
  Dim I As Integer
  
  pResult = Query("PRAGMA index_list('" & Table & "')")
  
  For I = 0 To GetResultCount(pResult) - 1
    aIndexes.Add(GetResultData(pResult, I, True)[1])
  Next
  
  FreeResult(pResult)
  
  Return aIndexes

End

Public Sub GetTableIndexInfo(Table As String, Index As String, ByRef Fields As String[], ByRef Unique As Boolean, ByRef Primary As Boolean)

  Dim pResult As Pointer
  Dim I As Integer
  Dim vRow As Variant[]

  pResult = Query("PRAGMA index_list('" & Table & "')")
  
  For I = 0 To GetResultCount(pResult) - 1
    vRow = GetResultData(pResult, I, True)
    If vRow[1] = Index Then
      Unique = vRow[2] <> "0"
      Primary = InStr(vRow[3], "p")
    Endif
  Next
  
  FreeResult(pResult)
  
  pResult = Query("PRAGMA index_info('" & Index & "')")
  
  Fields = New String[]
  For I = 0 To GetResultCount(pResult) - 1
    vRow = GetResultData(pResult, I, True)
    If vRow[2] Then Fields.Add(vRow[2])
  Next

  FreeResult(pResult)

End

Public Sub GetTablePrimaryKey(Table As String) As String[]
  
  Dim pResult As Pointer
  Dim I As Integer
  Dim vRow As Variant[]
  Dim sIndex As String
  Dim aFields As String[]

  pResult = Query("PRAGMA index_list('" & Table & "')")
  
  For I = 0 To GetResultCount(pResult) - 1
    vRow = GetResultData(pResult, I, True)
    If InStr(vRow[3], "p") Then
      sIndex = vRow[1]
      Break
    Endif
  Next
  
  FreeResult(pResult)
  
  If Not sIndex Then 
    
    ' [BM] If there is no primary key, we suppose that the first field of INTEGER datatype is the primary key.
    ' Because we use INTEGER only when creating the AUTOINCREMENT field.
    
    pResult = Query("PRAGMA table_info('" & Table & "')")
  
    For I = 0 To GetResultCount(pResult) - 1
      
      vRow = GetResultData(pResult, I, True)
      
      If vRow[2] = "INTEGER" Then
        aFields = [CStr(vRow[1])]
        Break
      Endif
      
    Next
  
    FreeResult(pResult)
    
    Return aFields
    
  Endif
  
  pResult = Query("PRAGMA index_info('" & sIndex & "')")
  
  aFields = New String[]
  For I = 0 To GetResultCount(pResult) - 1
    vRow = GetResultData(pResult, I, True)
    If vRow[2] Then aFields.Add(vRow[2])
  Next

  FreeResult(pResult)
  
  Return aFields

End

Public Sub GetCollations() As String[]
  
  $aCollations.ReadOnly = True
  Return $aCollations
  
End

Public Sub GetLastInsertId() As Long
  
  Dim pResult As Pointer
  Dim iLastId As Long
  
  pResult = Query("SELECT last_insert_rowid()")
  iLastId = GetResultData(pResult, 0, 0)[0]
  FreeResult(pResult)
  Return iLastId
  
End

Public Sub GetDatabases() As String[]
  
  Dim aDatabases As New String[]
  Dim sFile As String
  Dim sDir As String
  
  sDir = $sHost
  If Not sDir Then sDir = User.Home
    
  For Each sFile In Dir(sDir)
    If IsDatabaseFile(sDir &/ sFile) Then aDatabases.Add(sFile)
  Next
  
  Return aDatabases
  
End

Public Sub IsSystemDatabase((Database) As String) As Boolean
  
  Return False
  
End

Public Sub CreateDatabase(Database As String)

  Dim sPath As String
  Dim pSaveHandle As Pointer
  Dim pDatabase As Pointer
  
  sPath = FindDatabase($sHost, Database, True)
  If Not sPath Then Return

  pDatabase = _Sqlite3Helper.Open(sPath)
  If IsNull(pDatabase) Then Error.Raise("Unable to create database: " & sPath)

  pSaveHandle = $pHandle
  $pHandle = pDatabase
  
  Try Me.Exec("CREATE TABLE _gambas (field TEXT)")
  Try Me.Exec("DROP TABLE _gambas")
  
  _Sqlite3Helper.Close(pDatabase)
  
Finally 
  
  If pSaveHandle Then $pHandle = pSaveHandle

End

Public Sub DropDatabase(Database As String)

  Dim sPath As String

  sPath = FindDatabase($sHost, Database)
  If Not sPath Then Return 
  
  Kill sPath
  Try Kill sPath & "-shm"
  Try Kill sPath & "-wal"
  
End

Public Sub GetUsers() As String[]

  Return [User.Name]
  
End

Public Sub CreateUser((User) As String, (Password) As String, (Admin) As Boolean)

  Error.Raise("Cannot create user")
  
End

Public Sub DropUser((User) As String)
  
  Error.Raise("Cannot delete user")

End

Public Sub GetUserInfo((User) As String, ByRef Password As String, ByRef Admin As Boolean)
  
  Admin = True
  Password = ""
  
End

Public Sub SetUserPassword((User) As String, (Password) As String)

  Error.Raise("Cannot change password")

End

Public Sub CreateTable(Table As String, Fields As Field[], (Type) As String, PrimaryKey As String[])

  Dim sReq As String
  Dim hField As Field
  Dim bComma As Boolean
  Dim bNoPrimaryKey As Boolean

  sReq = "CREATE TABLE " & Me.Quote(Table, True) & " ("
  
  For Each hField In Fields
    
    If bComma Then 
      sReq &= ", "
    Else
      bComma = True
    Endif
    
    sReq &= Me.Quote(hField.Name) & " "
    
    Select Case hField.Type
      
      Case db.Serial
        sReq &= "INTEGER PRIMARY KEY AUTOINCREMENT"
        bNoPrimaryKey = True
      Case db.Blob
        sReq &= "BLOB"
      Case db.Boolean
        sReq &= "BOOL"
      Case db.Integer
        sReq &= "INT4"
      Case db.Long
        sReq &= "BIGINT"
      Case db.Float
        sReq &= "FLOAT8"
      Case db.Date
        sReq &= "DATETIME"
      Case db.String
        If hField.Length <= 0 Then
          sReq &= "TEXT"
        Else
          sReq &= "VARCHAR(" & CStr(hField.Length) & ")"
        Endif
      Case Else
        sReq &= "TEXT"
    End Select
    
    If hField.Collation Then sReq &= " COLLATION " & hField.Collation
    
    If Not IsNull(hField.Default) Then
      sReq &= " NOT NULL DEFAULT " & Me.Format(hField.Default)
    Else If PrimaryKey.Exist(hField.Name) Then
      sReq &= " NOT NULL"
    Endif
  
  Next
  
  If PrimaryKey.Count And If Not bNoPrimaryKey Then
    sReq &= ", PRIMARY KEY (" & Me.MakeFieldList(PrimaryKey) & ")"
  Endif

  sReq &= ")"
  
  Me.Exec(sReq)
  
End

