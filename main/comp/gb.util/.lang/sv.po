#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:32 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "Arabiska (Egypten)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "Arabiska (Tunisien)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr "Azerbajan (Azerbaijan)"

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "Bulgariska (Bulgarien)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "Katalanska(Katalonien, Spanien)"

#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr "Walesiska (Storbrittanien)"

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr "Tjeckiska (Tjeckien)"

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "Danska (Danmark)"

#: Language.class:40
msgid "German (Germany)"
msgstr "Tyska (Tyskland)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "Tyska (Belgien)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "Grekiska (Grekland)"

#: Language.class:47
msgid "English (common)"
msgstr "Engelska (Allmän)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "Engelska (Storbrittanien)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "Engelska (U.S.A.)"

#: Language.class:50
msgid "English (Australia)"
msgstr "Engelska (Austarlien)"

#: Language.class:51
msgid "English (Canada)"
msgstr "Engelska (Kananda)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr "Esperanto (Överallt!)"

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "Spanska (Spanien)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "Spanska (Argentina)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr "Estniska (Estland)"

#: Language.class:65
msgid "Basque (Basque country)"
msgstr "Baskiska (Baskien)"

#: Language.class:68
msgid "Farsi (Iran)"
msgstr "Farsi (Iran)"

#: Language.class:71
msgid "Finnish (Finland)"
msgstr "Finska (Finland)"

#: Language.class:74
msgid "French (France)"
msgstr "Franska (Frankrike)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "Franska (Belgien)"

#: Language.class:76
msgid "French (Canada)"
msgstr "Franska (Kanada)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "Franska (Schweitz)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "Galisiska(Spanien)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr "Hebreiska (Israel)"

#: Language.class:86
msgid "Hindi (India)"
msgstr "Hindi (Indien)"

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "Ungerska (Ungern)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "Kroatiska (Kroatioen)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "Indonesiska (Indonesien)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "Irländska (Irland)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

#: Language.class:104
msgid "Italian (Italy)"
msgstr "Italienska (Italien)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr "Japanska (Japan)"

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr "Khmer (Kambodja)"

#: Language.class:113
msgid "Korean (Korea)"
msgstr "Koeranska (Korea)"

#: Language.class:116
msgid "Latin"
msgstr "Latin"

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr "Litauiska (Litauen)"

#: Language.class:122
msgid "Malayalam (India)"
msgstr "Malaysiska (Indien)"

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr "Makedonska (Republiken Makedonien)"

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "Dutch (Nederländerna)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "Dutch (Belgien)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "Norska (Norge)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr "Punjabi (Indien)"

#: Language.class:138
msgid "Polish (Poland)"
msgstr "Polska (polen)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "Portugisiska (Porugal)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "Portugisiska (Brasilien)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr "Valensiska (Spanien)"

#: Language.class:148
msgid "Romanian (Romania)"
msgstr "Rumänska (Rumänien)"

#: Language.class:151
msgid "Russian (Russia)"
msgstr "Ryska (Ryssland)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "Slovenska (Slovenien)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr "Albanska (Albaien)"

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr "Serbiska (Serbien & Montenegro)"

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "Svenska (Sverige)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "Turkiska (Turkiet)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr "Uktainska (Ukraina)"

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr "Vietnamesiska (Vietnam)"

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "Vallonska (Belgien)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "Förenklad kinesiska (Kina)"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "Traditionell kinesiska (Taiwan)"

#: Language.class:241
msgid "Unknown"
msgstr "Okänd"
